﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;
    Vector3[] spawnPoints;

    private void Start()
    {
        spawnPoints = new Vector3[transform.childCount];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            spawnPoints[i] = transform.GetChild(i).transform.position;
        }

        m_currentCollectible = Instantiate(Collectible, spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)], Collectible.transform.rotation);
    }

    // Check if collectible has been destroyed
    // If it has been destoryed instantiate a new one at the position of one of the children of this transform (spawnpoints) chosen randomly.
    void Update()
    {
        if (!m_currentCollectible.activeSelf)
        {
            m_currentCollectible.SetActive(true);
            m_currentCollectible.transform.position = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)];
        }

    }
}
