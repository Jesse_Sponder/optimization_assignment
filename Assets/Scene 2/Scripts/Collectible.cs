﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        //assemble an array of colliders which are inside a sphere defined by the position of this object and a radius variable
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            //if a collider in the array is tagged "Player" this object will be destroyed
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                gameObject.SetActive(false);
            }
        }
    }

}
