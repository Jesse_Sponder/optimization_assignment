﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    Camera mainCamera;
    NavMeshAgent agent;
    private void Start()
    {
        // find object called "Main Camera" and hold it in camera variable
        mainCamera = GameObject.Find("Main Camera").GetComponent<Camera>();

        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // variable for raycast hit values
            RaycastHit hit = new RaycastHit();
            // while raycast is hitting objects in ground layer assign target desitination position for navmesh agent aquired from this gameobject
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                agent.destination = hit.point;
            }
        }
    }
}
