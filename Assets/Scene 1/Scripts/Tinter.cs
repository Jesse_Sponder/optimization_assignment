﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tinter : MonoBehaviour
{
    public Material[] materials; 
    // Use this for initialization
    void Start()
    {
        // - assigns a randomly generated r g b colour to material of object on start
        //GetComponentInChildren<Renderer>().material.color = new Color( UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ), UnityEngine.Random.Range( 0f, 1f ) );

        GetComponentInChildren<Renderer>().material = materials[(int)Random.Range(0, materials.Length)];
    }

}
