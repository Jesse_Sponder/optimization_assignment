﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{

    public GameObject Prefab;
    GameObject[] Spheres;
    int sphereIndex;
    public int numberOfSpheres; 
    public float FireSpeed = 5;

    private void Start()
    {
        Spheres = new GameObject[numberOfSpheres];

        for(int i = 0; i<Spheres.Length;i++)
        {
            GameObject prefabInstance = GameObject.Instantiate(Prefab, this.transform.position, Quaternion.identity, null);
            prefabInstance.SetActive(false);
            Spheres[i] = prefabInstance;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //When player presses key/ button associated with Fire1 input:
       
        if ( Input.GetButton( "Fire1" ) )
        {
            // - finds position in world space corresponding to pointer location on screen in front of camera by one unit
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            // - creates a vector pointed towards calculated point in world from the transform position of this gameobject
            Vector3 FireDirection = clickPoint - this.transform.position;
            // - set vector magnitude to 1
            FireDirection.Normalize();
            // - instantiates a prefab assigned in inspector at this gameobject's position
            // GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );

            if (sphereIndex >= Spheres.Length)
                sphereIndex = 0;
            if (!Spheres[sphereIndex].activeSelf)
                Spheres[sphereIndex].SetActive(true);

            Spheres[sphereIndex].transform.position = transform.position;
            // - assigns velocity of instantiated gameobject's rigidbody to created vector multiplied by publicly assigned FireSpeed
            Spheres[sphereIndex].GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
            sphereIndex++;
        }
    }

}
